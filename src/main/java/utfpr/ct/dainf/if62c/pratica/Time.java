/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import static java.util.Collections.sort;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 *
 * @author xadu
 */
public class Time{
    
    private HashMap<String,Jogador> jogadores = new HashMap<>();

    public HashMap<String, Jogador> getJogadores() {
        return jogadores;
    }
    
    public void addJogador(String k, Jogador v){
        jogadores.put(k, v);
    }
    
    public List<Jogador> ordena(JogadorComparator jc){
        //transforma em lista
        List<Jogador> time = new ArrayList<>(jogadores.values());
        //ordena
        Collections.sort(time, jc);
        //retorna
        return time;
    }
    
}
