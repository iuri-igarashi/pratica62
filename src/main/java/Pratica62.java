
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica62 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        ArrayList<Jogador> timeOrdenado;  
        
        Jogador player = new Jogador(1,"Fulano");
        time1.addJogador("Goleiro", player);
        
        player = new Jogador(1,"João");
        time2.addJogador("Goleiro", player);
        
        player = new Jogador(4,"Ciclano");
        time1.addJogador("Lateral", player);
        
        player = new Jogador(7,"José");
        time2.addJogador("Lateral", player); 
        
        player = new Jogador(10,"Beltrano");
        time1.addJogador("Atacante", player);
        
        player = new Jogador(15,"Mário");
        time2.addJogador("Atacante", player);
        
        //ordenacao
        JogadorComparator jc = new JogadorComparator(false, true, false);
        timeOrdenado = (ArrayList<Jogador>) time1.ordena(jc);
        
        System.out.print("");
        for(Jogador j : timeOrdenado) {
            System.out.println(j);
        }
        
        player = new Jogador(4,"Ciclano");
        time1.addJogador("Lateral", player);
        
        
    }
}
